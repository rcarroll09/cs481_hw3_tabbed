﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace HW3_Tabbed
{
    public partial class ColorsPage : ContentPage
    {
        
        public ColorsPage()
        {
            
            InitializeComponent();
            slider1.ValueChanged += (sender, args) =>
            {
                ColorChange.Color = Color.FromRgb(Convert.ToInt32(slider1.Value), Convert.ToInt32(slider2.Value), Convert.ToInt32(slider3.Value));
             
            };
            slider2.ValueChanged += (sender, args) =>
            {
                ColorChange.Color = Color.FromRgb(Convert.ToInt32(slider1.Value), Convert.ToInt32(slider2.Value), Convert.ToInt32(slider3.Value));

            };
            slider3.ValueChanged += (sender, args) =>
            {
                ColorChange.Color = Color.FromRgb(Convert.ToInt32(slider1.Value), Convert.ToInt32(slider2.Value), Convert.ToInt32(slider3.Value));

            };
        }
        void On_Appear(object sender, System.EventArgs e)
        {
            
            slider1.Value = App.r;
            slider2.Value = App.g;
            slider3.Value = App.b;
            ColorChange.Color = Color.FromRgb(Convert.ToInt32(App.r), Convert.ToInt32(App.g), Convert.ToInt32(App.b));
           

        }

        void On_Disappear(object sender, System.EventArgs e)
        {
            App.r = slider1.Value;
            App.g = slider2.Value;
            App.b = slider3.Value;
            

        }

    }
}
