﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabPage : TabbedPage
    {
        public TabPage()
        {
            InitializeComponent();

            NavigationPage foodpage = new NavigationPage(new FoodPage());
            foodpage.Title = "Food";

            Children.Add(new ColorsPage() { Title = "Colors" });
            Children.Add(new GamePage() { Title = "Game" });
            Children.Add(foodpage);
        }
    }
}
